<ul>
    <li><a href="/livro_felipes/{{$livro->id}}">{{ $livro->titulo }}</a></li>
    <li>{{ $livro->autor }}</li>
    <li>{{ $livro->isbn }}</li>
    <li>{{ $livro->tipo }}</li>
    <li>R$ {{ $livro->preco }}</li>
    <li>Cadastrado por {{ $livro->user->name ?? '' }}</li>
    <li><a href="/livro_felipes/{{$livro->id}}/edit">Editar</a></li>
    <li>
        <form action="/livro_felipes/{{ $livro->id }}" method="post">
            @csrf
            @method('delete')
            <button type="submit" onclick="return confirm('Tem certeza?');">Apagar</button> 
        </form>
    </li> 
</ul>