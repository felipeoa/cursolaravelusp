@extends('main')

@section('content')
    <form method="POST" action="/livro_felipes/{{ $livro->id }}">
        @csrf
        @method('patch')
        @include('livro_felipes.partials.form')
    </form>
@endsection