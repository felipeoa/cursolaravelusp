<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class LivroFelipeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = 
        [
            'titulo' => 'required',
            'autor'  => 'required',
            'isbn'   => ['required'],
            'tipo'   => ['required', Rule::in(\App\Models\LivroFelipe::tipos())],
            'preco'  => 'nullable'
        ];
        
        if ($this->method() == 'PATCH' || $this->method() == 'PUT')
        {
            array_push($rules['isbn'], 'unique:livro_felipes,isbn,'.$this->livro_felipe->id);
        }
        else
        {
            array_push($rules['isbn'], 'unique:livro_felipes');        
        }

        return $rules;
    }

    protected function prepareForValidation()
    {
        $this->merge(
        [
            'isbn' => preg_replace('/[^0-9]/', '', $this->isbn),
        ]);
    }
    
    public function messages()
    {
        return 
        [
            'isbn.unique' => 'Este isbn está cadastrado para outro livro',
        ];
    }
}