<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            'codpes'   => "5840128",
            'email'    => "felipeoa@usp.br",
            'name'     => "Felipe de Oliveira Arruda",
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'is_admin' => TRUE
        ];
        \App\Models\User::create($user);
        \App\Models\User::factory(10)->create();
    }
}
