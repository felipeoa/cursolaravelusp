@extends('main')

@section('content')
    <form method="get" action="/livro_felipes">
        <div class="row">
            <div class=" col-sm input-group">
            <input type="text" class="form-control" name="search" value="{{ request()->search }}">

            <span class="input-group-btn">
                <button type="submit" class="btn btn-success"> Buscar </button>
            </span>

            </div>
        </div>
    </form>
    <br/>

    @if (request()->search == '')    
        {{ $livros->appends(request()->query())->links() }}
    @endif
    <br/>

    @forelse ($livros as $livro)
        @include('livro_felipes.partials.fields')
    @empty
        Não há livros cadastrados
    @endforelse
@endsection