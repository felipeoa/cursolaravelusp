<?php

namespace Database\Factories;

use App\Models\LivroFelipe;
use Illuminate\Database\Eloquent\Factories\Factory;

class LivroFelipeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LivroFelipe::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $tipos = LivroFelipe::tipos();

        return 
        [
            'titulo' => $this->faker->sentence(3),
            'isbn'   => $this->faker->ean13(),
            'autor'  => $this->faker->name,
            'user_id' => \App\Models\User::factory()->create()->id,        
            'tipo' => $tipos[array_rand($tipos)],
        ];

    }
}
