<?php

namespace App\Http\Controllers;

use App\Models\LivroFelipe;
use Illuminate\Http\Request;
use App\Http\Requests\LivroFelipeRequest;
use Illuminate\Support\Facades\DB;

class LivroFelipeController extends Controller
{
    public function index(Request $request)
    {
        $this->authorize('logged');

        if(isset($request->search)) 
        {
            $livros = LivroFelipe::where('autor','LIKE',"%{$request->search}%")
                            ->orWhere('titulo','LIKE',"%{$request->search}%")->get();
        } 
        else 
        {
            $livros = LivroFelipe::paginate(5);
        }

        return view('livro_felipes.index',
        [
            'livros' => $livros
        ]);
    }

    public function create()
    {
        $this->authorize('admin');

        return view('livro_felipes.create',
        [
            'livro' => new LivroFelipe,
        ]);
    }

    public function store(LivroFelipeRequest $request)
    {
        $validated = $request->validated();
        $validated['user_id'] = auth()->user()->id;
        $livro = LivroFelipe::create($validated);
        request()->session()->flash('alert-info','Livro cadastrado com sucesso');        
        return redirect("/livro_felipes/{$livro->id}");
    }

    public function show(LivroFelipe $livro_felipe)
    {
        return view('livro_felipes.show',
        [
            'livro' => $livro_felipe
        ]);
    }

    public function edit(LivroFelipe $livro_felipe)
    {
        $this->authorize('admin');

        return view('livro_felipes.edit',
        [
            'livro' => $livro_felipe
        ]);
    }

    public function update(LivroFelipeRequest $request, LivroFelipe $livro_felipe)
    {
        $validated = $request->validated();
        $validated['user_id'] = auth()->user()->id;
        $livro_felipe->update($validated);
        request()->session()->flash('alert-info','Livro atualizado com sucesso');   
        return redirect("/livro_felipes/{$livro_felipe->id}");
    }

    public function destroy(LivroFelipe $livro_felipe)
    {
        $livro_felipe->delete();
        return redirect('/livro_felipes');
    }
}
