<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LivroFelipeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $livro = 
        [
            'titulo'  => "Macunaíma",
            'autor'   => "Mário de Andrade",
            'isbn'    => "9788520933619",
            'tipo'    => 'Nacional',
            'user_id' => 1
        ];
        
        \App\Models\LivroFelipe::create($livro);
        \App\Models\LivroFelipe::factory(15)->create();

    }
}
